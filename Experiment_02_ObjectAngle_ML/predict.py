import tensorflow as tf
import cv2
import numpy as np

loaded_model = tf.keras.models.load_model('runs/run3.keras')
image = cv2.imread('test_images/frame_5.png')
frame_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
frame_rgb = np.expand_dims(frame_rgb, axis=0)
predicted_angle = loaded_model.predict(frame_rgb)
print(f'Vorhergesagter Winkel: {predicted_angle} Grad')