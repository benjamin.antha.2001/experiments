import os
import cv2
import numpy as np
import tensorflow as tf
from keras import layers, models
import matplotlib.pyplot as plt

image_folder = ''
labels_file = 'labels.txt'

# Load labels from the labels file
labels = {}
with open(labels_file, 'r') as file:
    for line in file:
        parts = line.strip().split(':')
        image_name = parts[0].strip()
        label = int(parts[1].strip())
        labels[image_name] = label

# Load images and labels
images = []
class_labels = []
for image_name, label in labels.items():
    image_path = os.path.join(image_folder, image_name)
    image = cv2.imread(image_path)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)  # Convert BGR to RGB
    # image = cv2.resize(image, (image_height, image_width))  # Resize if needed
    images.append(image)
    class_labels.append(label)

# Convert lists to numpy arrays
images = np.array(images)
class_labels = np.array(class_labels)

# Normalize pixel values to be between 0 and 1
images = images / 255.0

model = models.Sequential([
    layers.Conv2D(32, (3, 3), activation='relu', input_shape=(300, 235, 3)),
    layers.MaxPooling2D((2, 2)),
    layers.Conv2D(64, (3, 3), activation='relu'),
    layers.MaxPooling2D((2, 2)),
    layers.Conv2D(128, (3, 3), activation='relu'),
    layers.MaxPooling2D((2, 2)),
    layers.Conv2D(64, (3, 3), activation='relu'),
    layers.MaxPooling2D((2, 2)),
    layers.Conv2D(128, (3, 3), activation='relu'),
    layers.MaxPooling2D((2, 2)),
    layers.Conv2D(64, (3, 3), activation='relu'),
    layers.Flatten(),
    layers.Dense(256, activation='relu'),
    layers.Dense(128, activation='relu'),
    layers.Dense(128, activation='relu'),
    layers.Dense(64, activation='relu'),
    layers.Dense(32, activation='relu'),
    layers.Dense(16, activation='relu'),
    layers.Dense(8, activation='relu'),
    layers.Dense(8, activation='relu'),
    layers.Dense(1)  # Output layer for angle prediction
])

# Compile the model
model.compile(optimizer='adam',
              loss='mean_squared_error',
              metrics=['mae'])

# Train the model
history = model.fit(images, class_labels, epochs=150, batch_size=3, validation_split=0.2)

# Plot training history
plt.plot(history.history['accuracy'], label='accuracy')
plt.plot(history.history['val_accuracy'], label='val_accuracy')
plt.xlabel('Epoch')
plt.ylabel('Accuracy')
plt.ylim([0.5, 1])
plt.legend(loc='lower right')
plt.show()

model.save("location.keras")