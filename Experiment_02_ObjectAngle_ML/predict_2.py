import numpy as np
from PIL import Image

import tensorflow as tf

# Load your trained model
model = tf.keras.models.load_model('runs/run7.keras')

# Load and preprocess the image
img_path = 'output_images/output_1.png'
img = Image.open(img_path)
img = img.resize((235, 300))  # Resize the image to match your model's input size
if img.mode != 'RGB':
    img = img.convert('RGB')
img = tf.keras.preprocessing.image.img_to_array(img)
img = np.expand_dims(img, axis=0)  # Add batch dimension
img = img/255.0  # Normalize pixel values if required

predicted_angle = model.predict(img) 

# Interpret predictions
print("Predicted angle (in degrees):", predicted_angle)
