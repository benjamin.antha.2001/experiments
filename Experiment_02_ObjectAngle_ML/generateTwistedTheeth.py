from PIL import Image
import os
import random

def overlay_images(image1_path, image2_path, output_path, position, rotation_angle, label, label_file):
    image1 = Image.open(image1_path)
    image2 = Image.open(image2_path)

    image1 = image1.rotate(rotation_angle, expand=True)

    paste_position = (position[0], position[1])

    image2.paste(image1, paste_position, image1)

    image2.save(output_path)

    label_file.write(f"{output_path}: {label}\n")

if __name__ == "__main__":
    image1_path = "../zaehne_bmp/Zahn_t_-4G.png" 
    image2_path = "../zaehne_bmp/schaft.png" 

    position = (30, 45)  

    rotation_angles = range(360) 

    output_dir = "output_images"
    os.makedirs(output_dir, exist_ok=True)

    # File to store labels
    labels_file_path = os.path.join(output_dir, "labels.txt")
    with open(labels_file_path, "w") as labels_file:
        for i in range(400):
            
            output_path = os.path.join(output_dir, f"output_{i}.png") 
            rotation_angle = int(random.uniform(0, 360))
            # Label for the generated image
            label = rotation_angle
            
            overlay_images(image1_path, image2_path, output_path, position, rotation_angle, label, labels_file)

    print("Images overlaid successfully!")
    print(f"Labels saved to {labels_file_path}")
