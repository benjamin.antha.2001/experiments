import os, shutil
import cv2
import numpy as np
import tensorflow as tf
from keras import layers, models, Input
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
import datetime

print("Num GPUS Available: ", len(tf.config.list_physical_devices('GPU')))

if os.path.isdir("logs"):
    shutil.rmtree("logs")

# Function to load images and corresponding labels from a folder
def load_data(image_folder, labels_file):
    images = []
    angles = []
    with open(labels_file, 'r') as file:
        for line in file:
            parts = line.strip().split(':')
            image_name = parts[0].strip()
            angle = float(parts[1].strip())
            image_path = image_name
            image = cv2.imread(image_path)
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            images.append(image)
            angles.append(angle)
    return np.array(images), np.array(angles)

# Define the paths to your image folder and labels file
image_folder = ''
labels_file = 'labels.txt'

# Load images and labels
images, angles = load_data(image_folder, labels_file)

# Normalize pixel values to be between 0 and 1
images = images / 255.0

# Split the data into training and testing sets
train_images, test_images, train_angles, test_angles = train_test_split(images, angles, test_size=0.2, random_state=42)

model = models.Sequential([
    Input(shape=(300, 235, 3)),
    layers.Conv2D(32, (3, 3), activation='relu', name="conv1"),
    layers.MaxPooling2D((2, 2), name="pool1"),
    layers.Conv2D(128, (3, 3), activation='relu', name="conv2"),
    layers.MaxPooling2D((2, 2), name="pool2"),
    layers.Conv2D(64, (3, 3), activation='relu', name="conv3"),
    layers.Flatten(name="flatten"),
    layers.Dense(256, activation='relu', name="dense1"),
    layers.Dense(128, activation='relu', name="dense2"),
    layers.Dense(64, activation='relu', name="dense3"),
    layers.Dense(1)  # Output layer for angle prediction
])

# Compile the model
model.compile(optimizer='adadelta',
              loss='mean_squared_error', 
              metrics=['mae'])  

log_dir = "logs/fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
tensorboard_callback =tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)


# Train the model
history = model.fit(train_images, train_angles, epochs=200,batch_size=10, validation_split=0.2, callbacks=[tensorboard_callback])

# Plot training results
plt.figure(figsize=(10, 5))

# Plot training & validation loss values
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('Model loss')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.legend(['Train', 'Validation'], loc='upper left')
plt.show()

# Evaluate the model on the test set
test_loss, test_mae = model.evaluate(test_images, test_angles)
print("Test MAE:", test_mae)
model.save("run4.keras")
