import cv2
import os

def save_frames(video_path, output_folder):
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    cap = cv2.VideoCapture(video_path)
    if not cap.isOpened():
        print("Error: Unable to open video file.")
        return
    
    fps = cap.get(cv2.CAP_PROP_FPS)
    frame_count = 0
    save_count = 0
    
    while True:
        ret, frame = cap.read()
        
        if not ret:
            break
        
        frame_count += 1
        
        if frame_count % 30 == 0:
            save_count += 1
            filename = os.path.join(output_folder, f"frame_{save_count}.png")
            cv2.imwrite(filename, frame)
            print(f"Frame {frame_count} saved as {filename}")
    
    cap.release()

if __name__ == "__main__":
    video_path = "videos/video_05.mp4"
    output_folder = "output_frames"
    
    save_frames(video_path, output_folder)
