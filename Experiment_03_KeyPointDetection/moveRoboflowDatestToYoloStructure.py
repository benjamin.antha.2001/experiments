import os
import shutil

source_root = "Annotation4Points"
destination_root = "YOLOv7-POSE/final_dataset"

def copy_files(source_dir, destination_dir, kind):
    for folder in os.listdir(source_dir):
        if os.path.isdir(os.path.join(source_dir, folder)):
            print(source_dir, folder)
            images_source = os.path.join(source_dir, folder)
            
            if folder == "images":
                images_destination = os.path.join(destination_dir, "images", kind)
            else:
                images_destination = os.path.join(destination_dir, "labels", kind)

            print(images_destination)
            os.makedirs(images_destination, exist_ok=True)
            
            for image_file in os.listdir(images_source):
                shutil.copy(os.path.join(images_source, image_file), os.path.join(images_destination, image_file))
            

copy_files(os.path.join(source_root, "train"), os.path.join(destination_root), "train")

copy_files(os.path.join(source_root, "valid"), os.path.join(destination_root), "val")

print("Files copied successfully.")
