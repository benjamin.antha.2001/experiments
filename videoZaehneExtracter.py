import torch
import cv2
import time
import os

video_path = "videos/video_05.mp4"
output_folder = "output_zaehne"
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
path = 'Experiment_01_ObjectDetection/tiny-model/best.pt'
model = torch.hub.load("Experiment_01_ObjectDetection/yolov7","custom",f"{path}",force_reload=True, source='local',trust_repo=True)
model.conf = 0.8
cap = cv2.VideoCapture('videos/video_05.mp4')  # Or use '0' for webcam

prev_frame_time = 0
new_frame_time = 0
fps = 0
save_count = 0
frame_count = 0
while cap.isOpened():
    ret, frame = cap.read()
    if not ret:
        break
    
    # convert color from bgr to rgb
    frame_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    # analyze frame with loaded model
    results = model(frame_rgb)
    # get coordinates for drawing the bounding boxes
    df = results.pandas().xyxy[0]
    
    frame_count += 1

    # draw the bounding boxes
    for _, row in df.iterrows():
        if frame_count % 30 == 0:
            save_count += 1
            roi_image = frame_rgb[int(row['ymin']):int(row['ymax']), int(row['xmin']): int(row['xmax'])]
            cv2.imshow("crop/region of interset image",roi_image) 
            filename = os.path.join(output_folder, f"zahn_{save_count}.png")
            cv2.imwrite(filename, roi_image)
            print(f"Frame {frame_count} saved as {filename}")

    cv2.imshow('Frame', frame_rgb)

    # calculate Frames
    new_frame_time = time.time()
    
    if (new_frame_time-prev_frame_time) != 0:
        fps = 1 / (new_frame_time-prev_frame_time)
        prev_frame_time = new_frame_time
        fps = int(fps)

    print(fps)
                
    
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
    

cap.release()
cv2.destroyAllWindows()
