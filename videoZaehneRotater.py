import torch
import cv2
import time
import os
import numpy as np

video_path = "videos/video_05.mp4"
output_folder = "rotated_zaehne"
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
path = 'Experiment_01_ObjectDetection/tiny-model/best.pt'
model = torch.hub.load("Experiment_01_ObjectDetection/yolov7","custom",f"{path}",force_reload=True, source='local',trust_repo=True)
model.conf = 0.8
cap = cv2.VideoCapture('videos/video_05.mp4')  # Or use '0' for webcam

prev_frame_time = 0
new_frame_time = 0
fps = 0
save_count = 0
frame_count = 0
while cap.isOpened():
    ret, frame = cap.read()
    if not ret:
        break
    
    # convert color from bgr to rgb
    frame_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    # analyze frame with loaded model
    results = model(frame)
    # get coordinates for drawing the bounding boxes
    df = results.pandas().xyxy[0]
    
    frame_count += 1

    # draw the bounding boxes
    for _, row in df.iterrows():
        if frame_count % 30 == 0:
            save_count += 1
            roi_image = frame[int(row['ymin']):int(row['ymax']), int(row['xmin']): int(row['xmax'])]
            
              # Rotate the roi_image at a custom angle (e.g., 45 degrees)
            angle = 45  # Set your custom angle here
            rows, cols = roi_image.shape[:2]
            rotation_matrix = cv2.getRotationMatrix2D((cols / 2, rows / 2), angle, 1)
            rotated_image = cv2.warpAffine(roi_image, rotation_matrix, (cols, rows))

            # Create a mask for the rotated region
            mask = np.ones_like(rotated_image) * 255
            
            # Rotate the mask
            rotated_mask = cv2.warpAffine(mask, rotation_matrix, (cols, rows))

            # Combine the rotated_image with the original frame using the mask
            frame[int(row['ymin']):int(row['ymax']), int(row['xmin']): int(row['xmax'])] = cv2.bitwise_and(rotated_image, rotated_mask) + cv2.bitwise_and(roi_image, cv2.bitwise_not(rotated_mask))

            cv2.imshow("crop/region of interest image", frame[int(row['ymin']):int(row['ymax']), int(row['xmin']): int(row['xmax'])]) 

            filename = os.path.join(output_folder, f"zahn_{save_count}.png")
            cv2.imwrite(filename, frame[int(row['ymin']):int(row['ymax']), int(row['xmin']): int(row['xmax'])])
            print(f"Frame {frame_count} saved as {filename}")

    cv2.imshow('Frame', frame)

    # calculate Frames
    new_frame_time = time.time()
    
    if (new_frame_time-prev_frame_time) != 0:
        fps = 1 / (new_frame_time-prev_frame_time)
        prev_frame_time = new_frame_time
        fps = int(fps)

    print(fps)
                
    
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
    

cap.release()
cv2.destroyAllWindows()
