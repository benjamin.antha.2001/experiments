import torch
import cv2
import time

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
path = 'tiny-model/best.pt'
model = torch.hub.load("yolov7","custom",f"{path}",force_reload=True, source='local',trust_repo=True)
model.conf = 0.8
cap = cv2.VideoCapture('videos/video_05.mp4')  # Or use '0' for webcam

prev_frame_time = 0
new_frame_time = 0
fps = 0

while cap.isOpened():
    ret, frame = cap.read()
    if not ret:
        break
    
    # convert color from bgr to rgb
    frame_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    # analyze frame with loaded model
    results = model(frame_rgb)
    # get coordinates for drawing the bounding boxes
    df = results.pandas().xyxy[0]
    # draw the bounding boxes
    for _, row in df.iterrows():
            cv2.rectangle(frame, (int(row['xmin']), int(row['ymin'])),(int(row['xmax']), int(row['ymax'])), (255,155,0), 2)
    cv2.imshow('Frame', frame)

    # calculate Frames
    new_frame_time = time.time()
    
    if (new_frame_time-prev_frame_time) != 0:
        fps = 1 / (new_frame_time-prev_frame_time)
        prev_frame_time = new_frame_time
        fps = int(fps)

    print(fps)
                
    
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
    

cap.release()
cv2.destroyAllWindows()
